---
date: 2020-09-01
title: Tasks in September 2020
service: backbone
---

## Service Agreement on Helmholtz Backbone
HIFIS fostered a service agreement with the German National Research and Education Network (DFN) on operating a a Helmholtz-wide Virtual Private Network (Helmholtz Backbone), covering the majority of Helmholtz centres. By September, the contract is expected to be signed from the participating partners.
