---
date: 2022-07-30
title: Tasks in July 2022
service: cloud
---

## Cloud Portal offers Service Categories and Keywords
Service categories and keywords categorise the Helmholtz Cloud Services and simplify the selection of a suitable service.
