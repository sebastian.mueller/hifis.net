---
layout: spotlight

###############################################################################
# Software spotlight Chemotion ELN
###############################################################################
# Templates starting with a _, e.g. "_template.md" will not be integrated into
# the spotlights.
#
# Optional settings can be left empty.

# -----------------------------------------------------------------------------
# Properties for spotlights list page
# -----------------------------------------------------------------------------

# The name of the software
name: Chemotion ELN

# Small preview image shown at the spotlights list page.
# Note: the path is relative to /assets/img/spotlights/
preview_image: chemotion_ELN/chemotion_logo_full.svg

# One or two sentences describing the software
excerpt: Chemotion ELN is an Open Source electronic lab notebook (ELN) for scientists working in chemistry and colleagues from neighboring disciplines, developed and updated at KIT. The web-based application allows the acquisition, management, storage, processing, and sharing of research data.

# -----------------------------------------------------------------------------
# Properties for individual spotlights page
# -----------------------------------------------------------------------------
# Entries here will be shown in the green box on the right of the screen.

# Jumbotron (optional)
# The path is relative to /assets/img/jumbotrons/
title_image:

# Title at the top, inside the title-content-container
title: Chemotion ELN - an open source Electronic Lab Notebook for Chemistry

# Add at least one keyword
keywords:
    - Electronic Lab Notebook
    - Chemistry
    - FAIR Data
    - Experimental Sciences

# The Helmholtz research field
hgf_research_field: Information

# At least one responsible centre
hgf_centers:
    - "KIT"

# Other contributing organisations (optional)
contributing_organisations:

# List of scientific communities
scientific_community:
    - Organic Chemistry

# Impact on community (optional, not implemented yet)
impact_on_community:

# An e-mail address
contact: nicole.jung@kit edu

# Platforms (optional)
# Provide platforms in this format
#   - type: TYPE
#     link_as: LINK
# Valid TYPES are: webpage, telegram, mailing-list, twitter, gitlab, github
# Mailing lists should be added as "mailto:mailinglist@url.de"
# More types can be implemented by modifying /_layouts/spotlight.html
platforms:
    - type: github
      link_as: https://github.com/ComPlat/chemotion_ELN
    - type: webpage
      link_as: https://www.chemotion.net/

# The software license (optional)
license: GPL-3.0

# Is the software pricey or free? (optional)
costs:

# What is this software used for in general (e.g. modelling)? (optional, not implemented yet)
software_type:
    - Research Data Management

# The application type (Desktop, Mobile, Web) (optional, not implemented yet)
application_type:
    -

# List of programming languages (optional)
programming_languages:
    - Ruby, JavaScript, ReactJS

# DOI (without URL, just 10.1000/1.0000000 ) (optional)
doi: 10.1186/s13321-017-0240-0

# Funding of the software (optional)
funding:
    - shortname: KIT
    - shortname: DFG
    - shortname: State of Baden-Württemberg
---

# Chemotion ELN - Electronic Laboratory Notebook & Repository for Research Data

[Chemotion ELN](https://www.chemotion.net) is an Open Source electronic lab notebook (ELN) for scientists working in chemistry and colleagues from neighboring disciplines, developed and updated at KIT. The web-based application allows the acquisition, management, storage, processing, and sharing of research data. The ELN provides access to various functions, helper tools, and external sources of information that facilitate the work with data and their analysis. In addition, chemotion allows one of the most important improvements regarding sustainable scientific work: It supports chemistry researchers in academia to build their digital information databases as a prerequisite for the detailed, systematic investigation and evaluation of chemical reactions, chemical compounds, and related data.

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/chemotion_ELN/chemotion_ELN.png" alt="The graphical user interface of Chemotion ELN">
<span>The screenshot shows an exemplary graphical user interface of Chemotion ELN.</span>
</div>

The software was built to offer a digitalization strategy for experimental sciences, contributing to the enhancement of FAIR and open data wherever possible. Therefore, the ELN includes concepts to transfer data and metadata from the instruments used for the experiments, and it offers interfaces to transfer information to repositories for research data. The interoperable transfer of research data is realized with the open access repository chemotion. Taken together, the chemotion systems offer a comprehensive research infrastructure for FAIR data management in chemistry, from the acquisition of data to their publication.

# Watch Video:
[<i class="fas fa-external-link-alt"></i> Electronic Laboratory Notebook (ELN) & Repository for Research Data](https://www.youtube.com/watch?v=tZHaP6DW-Dw) (_link to Youtube_)
