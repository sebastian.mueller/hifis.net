---
title: Version Control using Git (CANCELLED)
layout: event
organizers:
  - belayneh
lecturers: 
  - belayneh
  - dolling
  - "Fuchs, Hannes"
  - "Lüdtke, Stefan"
  - "Meeßen, Christian"
type:   workshop
start:
    date:   "2021-07-26"
    time:   "09:00"
end:
    date:   "2021-07-27"
    time:   "14:00"
location:
    campus: "Online"
excerpt:
    "This workshop will teach version control using Git for scientists and PhD students."
redirect_from:
  - events/2021/03/16/2021-07-26and27-version-control-using-git
  - events/2021/03/17/2021-07-26and27-version-control-using-git
---

## Goal

Introduce scientists and PhD students to the use of version control in research environment.

## Content

The workshop covers introductory topics on the use of version control in research, as well as hands on sessions on different functionalities of Git with GitLab. You will find more information on [the workshop page]().


## Requirements

Neither prior knowledge nor experience in those tools is needed.
A headset (or at least headphones) is required.

**This workshop has been cancelled!**

