---
title: "Cancelled: Project management with GitLab"
layout: event
organizers:
  - erxleben
lecturers:
  - TBA
type:   workshop
start:
    date:   "2021-12-10"
end:
    date:   "2021-12-13"
registration_link:
location:
    campus: online
fully_booked_out: true
registration_period:
    from:   "2021-01-01"
    to:     "2021-01-01"
excerpt:
    This event has been cancelled - Sorry about that.
    We will offer a replacement as soon as possible.
---

## Note

The event has been cancelled.
We will offer a replacement as soon as possible.

## Goal

The workshop is aimed at those who have to manage development projects in a research context.
It introduces a workflow and best practises for day-to-day operations
with the aim to increase productivity, overall quality and make everyones' life a bit easier.

## Content

This course is split into two parts:

1. Introduction to version control using the tool Git
2. Project Management with the Web-Platform [GitLab](https://about.gitlab.com)

Version Control is an essential building block in managing digital projects of any scale and enabling successful collaboration.
Building on top of this, an advanced project management system provides further potential for enhanced productivity.

## Requirements

No previous knowledge in the covered topics will be required.
Participants are advised to have a computer on which they can install software.
