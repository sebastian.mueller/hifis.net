---
layout: spotlight

# Spotlight list attributes
name: ODV
preview_image: odv_plus/odv5-logo.jpg
excerpt: Ocean Data View (ODV) is a software package for the interactive exploration, analysis and visualization of oceanographic and other geo-referenced environmental data. ODV runs on Windows, MacOS, and Linux. ODV data and view files are platform-independent and can be exchanged between different systems.

# Title for individual page
title_image:
title: Ocean Data View ODV
keywords:
    - Data Analysis
    - Data Visualization
    - Environmental Data
hgf_research_field: Earth and Environment
hgf_centers:
    - AWI Bremerhaven
contributing_organisations:
scientific_community: Oceanography / Meteorology / Climate Research
impact_on_community:
contact: reiner.schlitzer@awi.de
platforms:
    - type: Native application (Windows, MacOS, Linux)
      link_as: https://odv.awi.de/
    - type: Webpage
      link_as: https://odv.awi.de/
    - type: mailing-list
      link_as: mailto:odv@awi.de
license: Proprietary
costs: Free
software_type:
    - Native application
application_type:
    - Executable
programming_languages:
    - C++
doi:
funding:
    - shortname: AWI
      link_as: https://awi.de  # Optional
    - shortname: EU Horizon2020
---

# ODV in a nutshell

ODV displays original data points or gridded fields based on the original data. ODV has two fast weighted-averaging gridding algorithms as well as the advanced DIVA gridding software built-in. Gridded fields can be color-shaded and/or contoured. ODV supports five different map projections and can be used to produce high quality cruise maps. ODV graphics output (see example below) can be sent directly to printers or may be exported to PostScript, gif, png, or jpg files. The resolution of exported graphics files is specified by the user and not limited by the pixel resolution of the screen.

The ODV data format allows dense storage and very fast data access. Large data collections with millions of stations can easily be maintained and explored on inexpensive desktop and notebook computers. Data from Argo, GTSPP, CCHDO, CORA, World Ocean Database, World Ocean Atlas, World Ocean Circulation Experiment (WOCE), SeaDataNet, and Medar/Medatlas can be directly imported into ODV. Ready-to-use versions of the WOCE data, the gridded World Ocean Atlas as well as many other important geoscience datasets are available for download.

ODV also supports the netCDF format and lets you explore and visualize CF compliant netCDF datasets. This works with netCDF files on your local machine as well as with remote netCDF files served by an OPeNDAP server.

## webODV - ODV going online

webODV is the online version of ODV running in the browser without the need to install any additional software. webODV is perfectly suited to provide community datasets to serve always the newest versions and releases. In addition to the ODV-online, the nearly 1:1 ODV implementation, an intuitive data extraction service is available. Productive webODV instances are running at [geotraces.webodv.awi.de](https://geotraces.webodv.awi.de), [emodnet-chemistry.webodv.awi.de](https://emodnet-chemistry.webodv.awi.de/) and [explore.webodv.awi.de](https://explore.webodv.awi.de/).

<div class="spotlights-text-image">
<img src="{{ site.directory.images | relative_url}}spotlights/odv_plus/odv_section.jpg" alt="Example ODV section plot">
<span>An example of an ODV section plot.</span>
</div>
