/assign @erxleb87
/assign @hueser93
/label ~"Progress::ToDo"

>>>
The following will trigger a notification to the software.hifis.net core team (just to make sure)
>>>

@software-hifis-net Please add me as a member.

# Request to add Team Member
>>>
This template can be used by people who want to be added as HIFIS Software team
members but do not feel comfortable with manipulating YAML themselves.
Just fill at least the required information.
>>>

## Required Information
### Family Name(s)

### First Name(s)

### Academic Title
>>>
  If applicable.
>>>

### Organization
>>>
  Check one. If you are not located on a main campus, please provide the 
  address of the sub-campus.
>>>

* [ ] AWI
* [ ] DESY
* [ ] DKFZ
* [ ] DLR
* [ ] FZJ
* [ ] GFZ
* [ ] HMGU
* [ ] HZB
* [ ] HZDR
* [ ] KIT
* [ ] UFZ

### Position 
>>>
Short job description, e.g. Research Engineer
>>>

### Rank
0 
>>>
  Used for sorting. Increase for every hierarchy level below you. 
  Rough Guideline:
  Team Member = 0
  Team Leader = 1
  Cluster Coordinator = 2
  Platform Coordinator = 3
>>>

## Optional Information

### Image
>>>
  Please also provide an image to be put on the new team member page to the 
  actual assignee of this issue.
  These are our recommendations which we check before we put the suggested
  image online:
  1. File size should not be more than 40 KByte
  2. Image should be square with a preferred resolution of 400px x 400px
  3. Common web formats like _jpeg_ or _png_ are preferred
>>>

### Office Location 
>>>
  On campus, used to refine the visitors/postal address
  e.g. Building C, Room 314.
>>>

## Contacts
>>>
  Providing any contacts is optional.
  A postal address of your campus will be provided as fallback.
>>>

### Email

### Phone

>>>
In case you would like to provide a phone number please add the
international calling code as well.
>>>

### SMS

>>>
Mobile phone numbers given here are used to open an SMS client
directly on users' mobile phones.
>>>

### Fax

### Messengers
>>>
  List the messenger name and contact link
>>>

### Social Platforms
>>>
  List the platform name and contact link
>>>
