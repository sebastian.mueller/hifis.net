---
date: 2021-01-15
title: Tasks in January 2021
service: software
---

## Publish Helmholtz Hacky Hour plans for the first half of 2021 
The community work package publishes further details for the Helmholtz Hacky Hour for the first half of 2021 on the [events page](https://software.hifis.net/events).
